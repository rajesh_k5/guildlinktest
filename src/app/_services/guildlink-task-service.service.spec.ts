import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { GuildlinkTaskServiceService } from './guildlink-task-service.service';

describe('GuildlinkTaskServiceService', () => {
  let service: GuildlinkTaskServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(GuildlinkTaskServiceService);
    
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  
});

