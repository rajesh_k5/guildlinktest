import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GuildlinkTaskServiceService {

  apiRoot = environment.apiRoot;;
  constructor(private httpClient:HttpClient) { }


 getWorkingDay(startDate:any,endDate:any,jurisdiction:any,isIncluded:any): Observable <any> 
 {
  const httpOptions = {
    params: new HttpParams().
    set("startDate",startDate ).
    set("endDate",  endDate)
    .set('jurisdiction',jurisdiction).set("isIncluded",  isIncluded)
  };

  return this.httpClient.get(this.apiRoot,httpOptions)
 }
  
}
