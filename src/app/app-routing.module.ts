import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkingDayComponent } from './_components/working-day/working-day.component';


const routes: Routes = [

  { path: '', pathMatch: 'full', redirectTo: 'working-days' },
  { path: 'working-days', component: WorkingDayComponent },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
