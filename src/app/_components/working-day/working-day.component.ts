import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { State } from 'src/app/_common/common.interface';
import { GuildlinkTaskServiceService } from 'src/app/_services/guildlink-task-service.service';

@Component({
  selector: 'app-working-day',
  templateUrl: './working-day.component.html',
  styleUrls: ['./working-day.component.scss']
})
export class WorkingDayComponent implements OnInit {
  
  submitted = false;
  dateForm: FormGroup;
  workingDays:any;
  dayCount:any;
  startDate: any;
  defultDate = new Date();
  endDate:any;
  jurisdiction:any;
  dateFilter: any;
  state: State[] = [
    {value: 'nsw', viewValue: 'NSW'},
    {value: 'act', viewValue: 'ACT'},
    {value: 'qld', viewValue: 'QLD'}
  ];

  constructor(private fb: FormBuilder,
    private guildlinkService:GuildlinkTaskServiceService,
    private datePipe : DatePipe,
    private spinner: NgxSpinnerService) {

      this.dateForm = this.fb.group({
        fromDate: [this.datePipe.transform(this.defultDate,'yyyy-MM-dd'), Validators.required],
        endDate:  [this.datePipe.transform(this.defultDate,'yyyy-MM-dd'), Validators.required],
        jurisdiction:['',Validators.required],
        isDateIncludes:[false]

        
      });
    
   }

  ngOnInit(): void {
  }

onSubmit()
{
  this.submitted = true;
  if (this.dateForm.invalid) {
    return;
}
  this.getWorkingDays();
}




getWorkingDays() {
this.spinner.show();
this.startDate=this.datePipe.transform(this.dateForm.controls['fromDate'].value, 'yyyy-MM-dd');
this.endDate=this.datePipe.transform(this.dateForm.controls['endDate'].value, 'yyyy-MM-dd');
this.jurisdiction=this.dateForm.controls['jurisdiction'].value;
this.dateFilter=this.dateForm.controls['isDateIncludes'].value;
this.workingDays= this.guildlinkService.
getWorkingDay(this.startDate,this.endDate,this.jurisdiction,this.dateFilter).

subscribe((res) => {
  this.spinner.hide();
  console.log(res)
     this.workingDays=res;
     this.dayCount='Number of Weekdays for '+ this.jurisdiction.toUpperCase() +':' +this.workingDays.content;
}, err => {
  this.spinner.hide();
  console.log(err.message);
  this.dayCount=err.message;
});
}
}