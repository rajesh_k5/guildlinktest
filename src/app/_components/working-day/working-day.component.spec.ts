import { OverlayModule } from '@angular/cdk/overlay';
import { DatePipe } from '@angular/common';
import {  HttpClientModule } from '@angular/common/http';
import { isExpressionFactoryMetadata } from '@angular/compiler/src/render3/r3_factory';
import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { GuildlinkTaskServiceService } from 'src/app/_services/guildlink-task-service.service';

import { WorkingDayComponent } from './working-day.component';

describe('WorkingDayComponent', () => {
  let component: WorkingDayComponent;
  let _fixture: ComponentFixture<WorkingDayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    declarations: [ WorkingDayComponent ],
    imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatCheckboxModule,
    NgxSpinnerModule
      ],
      providers: [ 
          GuildlinkTaskServiceService,DatePipe
        
       ]
    })
    .compileComponents()
  });

  beforeEach(() => {
    _fixture = TestBed.createComponent(WorkingDayComponent);
    component = _fixture.componentInstance;
    component.ngOnInit();
    _fixture.detectChanges();
});


 
 
 
it('form should be invalid',() => {
  console.log("component",component.dateForm);
  component.dateForm.controls['fromDate'].setValue(null);
  component.dateForm.controls['endDate'].setValue(null);
  component.dateForm.controls['jurisdiction'].setValue('');
  component.dateForm.controls['isDateIncludes'].setValue(false);
  expect(component.dateForm.valid).toBeFalsy();
  _fixture.detectChanges();
  let btn = _fixture.debugElement.query(By.css('button'));  // fetch button element
  expect(btn.nativeElement.disabled).toBeFalse();  
});

  
it('should have form valid input', () => {
 // component.onSubmit();
  component.dateForm.controls['fromDate'].setValue(new Date());
  component.dateForm.controls['endDate'].setValue(new Date());
  component.dateForm.controls['jurisdiction'].setValue('nsw');
  component.dateForm.controls['isDateIncludes'].setValue(false);
  expect(component.dateForm.valid).toBeTruthy();
});
 
it('should call submit button', () => {
  spyOn(component, 'onSubmit');
  let button = 
  _fixture.debugElement.nativeElement.querySelector('button');
  button.click();
  _fixture.whenStable().then(() => {
    expect(component.onSubmit).toHaveBeenCalled();
  });
});

it('should call get getWorkingDays function', () => {
  spyOn(component, 'getWorkingDays');
  component.getWorkingDays();
  expect(component.getWorkingDays).toHaveBeenCalled();
})
});



